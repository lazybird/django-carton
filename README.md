
Django Carton
=============

Django Carton is hosted [here][github].


      +------+
     /|     /|
    +-+----+ |    django-carton is a simple and lightweight application
    | |    | |    for shopping carts and wish lists.
    | +----+-+
    |/     |/
    +------+




[github]: https://github.com/lazybird/django-carton/
